import { AppComponent } from './app.component'

import 'rxjs/add/observable/empty'
import 'rxjs/add/observable/from'

describe('AppComponent', () => {
  let component: AppComponent

  beforeEach(() => {
    component = new AppComponent()
    component.ngOnInit()
  })

  it('should add new user to array', () => {
    const state = false

    component.addNewUserStatus(state)

    expect(component.addNewUserStatus(state)).toBe(state)
  })
})
