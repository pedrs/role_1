import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core'
import { UsersService } from './../users.service'


@Component({
  selector: 'app-user-new',
  templateUrl: './user-new.component.html',
  styleUrls: ['./user-new.component.css'],
  providers: [UsersService]
})
export class UserNewComponent implements OnInit {
  newUserStatus: boolean
  positions: object


  @Output() addNewUserStatus: EventEmitter<boolean> = new EventEmitter()

  constructor(private userService: UsersService) {}

  ngOnInit() {

  }

  addNewUser(state): void {
    this.addNewUserStatus.emit(state)
    this.newUserStatus = state
  }

  addUser(data): any {
    this.userService.addUser(data)
  }

  getPositions() {
    this.userService
      .getPositions()
      .subscribe(positions => (this.positions = positions))
  }
}
