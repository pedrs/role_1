import { Observable } from 'rxjs/Observable'
import { UsersService } from './../users.service'
import { UserNewComponent } from './user-new.component'
import 'rxjs/add/observable/from'

describe('UserNewComponent', () => {
  let component: UserNewComponent
  let service: UsersService
  const data: object = {
    id: 1,
    name: 'Jan',
    surName: 'Veliký',
    position: 'product manager',
    birth: '1.1.1980'
  }
  beforeEach(() => {
    service = new UsersService(null)
    component = new UserNewComponent(service)
  })

  it('should set positions property with the items returned from the server ', () => {
    const positions = {
      positions: [
        'full-stack developer',
        'front-end developer',
        'sw admin',
        'help desk',
        'scrum master',
        'product manager'
      ]
    }
    spyOn(service, 'getPositions').and.callFake(() => {
      return Observable.from([positions])
    })

    component.ngOnInit()

    expect(component.positions).toBe(positions)
  })

  it('should inform that we can add new user and set up the rule', () => {
    let addNewuser: boolean

    component.addNewUserStatus.subscribe(add => (addNewuser = add))

    component.addNewUser(false)

    expect(addNewuser).toBe(false)
  })

  it('should send data which we can add to array with users', () => {
    const spy = spyOn(service, 'addUser').and.callFake(t => {
      return Observable.empty()
    })

    component.addUser(data)

    expect(spy).toHaveBeenCalled()
  })
})
