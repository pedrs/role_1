import {Users} from './USERS_INTERFACE';

export const USERS: Users[] = [
  {
    id: '1',
    name: 'Petr',
    surName: 'Novak',
    position: 'sw admin',
    birth: '22.4.1978'
  },
  {
    id: '2',
    name: 'Petr',
    surName: 'Nový',
    position: 'scrum master',
    birth: '22.4.1978'
  },
  {
    id: '3',
    name: 'Pavel',
    surName: 'Malý',
    position: 'scrum master',
    birth: '22.4.1988'
  }
]
