import { of } from 'rxjs'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient } from '@angular/common/http'
import { USERS } from './USERS'


@Injectable()
export class UsersService {
  users = USERS

  private positionsUrl = 'http://ibillboard.com/api/positions'

  constructor(private http: HttpClient) {}

  getPositions(): Observable<object> {
    return this.http.get(this.positionsUrl)
  }

  getUsers(): Observable<any> {
    return of(this.users)
  }

  addUser(data) {
    const id = (this.users.length + 1).toString()
    const user = { id, ...data }
    this.users.push(user)
  }

  delete(id): any {
    const item = this.users.findIndex(value => value.id === id)
    const alert = confirm('Are you sure?')
    if (alert) {
      this.users.splice(item, 1)
    }
  }

  saveUsers(data): any {
    return Object.assign({}, this.users, data)
  }
}
