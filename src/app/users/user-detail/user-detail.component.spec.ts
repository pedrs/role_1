import { UsersService } from './../users.service'
import { Observable } from 'rxjs'
import { UserDetailComponent } from './user-detail.component'

import 'rxjs/add/observable/from'
import 'rxjs/add/observable/empty'






describe('UserDetailComponent', () => {
  let component: UserDetailComponent
  let service: UsersService
  const data = {
    id: '4',
    name: 'Jan',
    surName: 'Veliký',
    position: 'product manager',
    birth: '1.1.1980'
  }
  beforeEach(() => {
    service = new UsersService(null)
    component = new UserDetailComponent(service)
  })

  it('should set positions property with the items returned from the server ', () => {
    const positions = {
      positions: [
        'full-stack developer',
        'front-end developer',
        'sw admin',
        'help desk',
        'scrum master',
        'product manager'
      ]
    }
    spyOn(service, 'getPositions').and.callFake(() => {
      return Observable.from([positions])
    })

    component.ngOnInit()

    expect(component.positions).toBe(positions)
  })

  it('should NOT call the server to delete a user item if the user cancels', () => {
    const spy = spyOn(component, 'deleteUser').and.returnValue(false)

    spyOn(window, 'confirm').and.returnValue(false)

    component.deleteUser(1)

    expect(spy).toHaveBeenCalled()
  })

  it('should call the server to delete a user item if the user confirms', () => {
    const spy = spyOn(service, 'delete').and.returnValue(Observable.empty())

    spyOn(window, 'confirm').and.returnValue(true)

    component.deleteUser(1)

    expect(spy).toHaveBeenCalled()
  })

  it('should save edited user ', () => {
    const spy = spyOn(service, 'saveUsers').and.returnValue(Observable.empty())

    component.saveUsers(data)

    expect(spy).toHaveBeenCalled()

    expect(service.getUsers().subscribe.length).toEqual(3)
  })

  it('should inform component if we edit user we can user right choice user', () => {
    const state = false

    component.editUserStatus(state)

    expect(component.editUserStatus(state)).toEqual(state)
  })
})
