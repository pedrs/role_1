import { UsersService } from './../users.service'
import { Component, OnInit, Input } from '@angular/core'
import {Users} from '../USERS_INTERFACE';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css'],
  providers: [UsersService]
})
export class UserDetailComponent implements OnInit {
  @Input() user: Users
  @Input () positions: object
  @Input() newUserStatus: boolean

  editUsersStatus: boolean


  constructor(private userService: UsersService) {}


  ngOnInit() {

  }

  saveUsers(data): object {
    this.editUsersStatus = false
    return this.userService.saveUsers(data)
  }

  deleteUser(id): object {
   return this.userService.delete(id)
  }

  editUserStatus(state): boolean {
    return (this.editUsersStatus = state)
  }


}
