import { UsersService } from './../users.service'
import { Component, Input, OnInit } from '@angular/core'
import {Users} from '../USERS_INTERFACE';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css'],
  providers: [UsersService]
})
export class UsersListComponent implements OnInit {
  title = 'List of users'

  users: Users
  positions: object

  @Input() newUserStatus: boolean

  constructor(private userService: UsersService) {}
  ngOnInit() {
    this.getUsers()
    this.getPositions()
  }


  getUsers () {
    this.userService.getUsers().subscribe(users => (this.users = users))
  }

  getPositions() {
    return this.userService
      .getPositions()
      .subscribe(positions => (this.positions = positions))
  }
}
