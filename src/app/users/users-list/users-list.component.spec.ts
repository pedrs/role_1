import { UsersService } from './../users.service'
import { Observable } from 'rxjs/Observable'
import { UsersListComponent } from './users-list.component'
import { USERS } from './../USERS'
import 'rxjs/add/observable/from'

describe('UsersListComponent', () => {
  let component: UsersListComponent
  let service: UsersService
  const users = USERS
  beforeEach(() => {
    service = new UsersService(null)
    component = new UsersListComponent(service)
  })

  it('should set positions property with the items returned from the server ', () => {
    spyOn(service, 'getUsers').and.callFake(() => {
      return Observable.from([users])
    })
    component.ngOnInit()

    expect(component.users)
  })
})
