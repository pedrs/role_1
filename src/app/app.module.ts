import { UsersService } from './users/users.service';



import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';



import { AppComponent } from './app.component';
import { UsersListComponent } from './users/users-list/users-list.component';
import { UserNewComponent } from './users/user-new/user-new.component';
import { HeaderComponent } from './header/header.component';
import { UserDetailComponent } from './users/user-detail/user-detail.component';



@NgModule({
  declarations: [
    AppComponent,
    UsersListComponent,
    UserNewComponent,
    HeaderComponent,
    UserDetailComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
